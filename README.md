# TUST 机器学习 线性分类器课程实验

## 介绍

对国内汽车牌号文本进行识别

实验方案流程：

1. 创建需要训练的图像数据集
2. 将每张图像进行规格化，对图像二值化、边缘裁减并缩放到30*30像素
3. 对每张图像切分为5*5像素的小图像，计算1值像素占总像素比值，作为样本参数，构成36维样本向量
4. 使用线性分类器（Fisher或感知准则函数）对任意两张图像的所有组合进行训练，得到模型数据
5. 对测试图像进行识别，先将图像规格化得到样本向量，再将样本向量应用到每个模型数据的判别项进行分类，对于分类的结果进行计分，得到分数列表，再对列表进行排序，得到可能性最大的分类结果

线性分类器实验相关数据和程序包括fisher分类器程序、字体文件、测试数据和其他必要的工具。

* `label.txt` 用于确定训练和分类所需的字符集合
* `fonts/` 存放字体文件，包括流行的中文字体和拉丁文字体
* `makeimgs.py` 生成图像数据集，输入字体目录，通过label 文件指定字符集
* `preprocess.py` 生成训练样本，输入图像数据所在路径，输出样本参数文件
* `train_lda.py` 调用线性分类器对样本训练，输出模型参数文件，使用`-t`参数制定Fisher算法或感知准则函数
* `test.py` 执行测试用例，输入要分类的图像路径和模型参数文件，对图像进行识别

具体使用方式参考 `Makefile`

## 执行测试

```
$ make
```

## 实验结果

Fisher

```
test/!.png --> '!'
test/#.png --> '#'
test/$.png --> '$'
test/&.png --> '&'
test/+.png --> '+'
test/0.png --> '0'
test/1.png --> '1'
test/2.png --> '2'
test/3.png --> '3'
test/4.png --> '4'
test/5.png --> '5'
test/6.png --> '6'
test/7.png --> '7'
test/8.png --> '8'
test/9.png --> '9'
test/=.png --> '='
test/?.png --> '?'
test/AA.png --> 'A'
test/BB.png --> '苏'
test/CC.png --> 'C'
test/DD.png --> 'D'
test/EE.png --> 'E'
test/FF.png --> 'F'
test/GG.png --> 'G'
test/HH.png --> 'H'
test/II.png --> '1'
test/JJ.png --> 'J'
test/KK.png --> 'K'
test/LL.png --> 'L'
test/MM.png --> 'M'
test/NN.png --> 'N'
test/OO.png --> 'O'
test/PP.png --> 'P'
test/QQ.png --> 'Q'
test/RR.png --> 'R'
test/SS.png --> 'S'
test/TT.png --> 'T'
test/UU.png --> 'U'
test/VV.png --> 'V'
test/WW.png --> 'w'
test/XX.png --> 'x'
test/YY.png --> 'Y'
test/ZZ.png --> 'z'
test/^.png --> '^'
test/a.png --> 'a'
test/b.png --> 'b'
test/c.png --> 'c'
test/chuan.png --> '川'
test/comma.png --> 'm'
test/d.png --> 'd'
test/ee.png --> '甘'
test/f.png --> 'f'
test/g.png --> '桂'
test/gan.png --> '甘'
test/gt.png --> '>'
test/gui.png --> '桂'
test/gui4.png --> '贵'
test/h.png --> 'h'
test/hei.png --> '黑'
test/hg.png --> '-'
test/hu.png --> '沪'
test/i.png --> 'i'
test/j.png --> 'j'
test/ji2.png --> '吉'
test/ji4.png --> '冀'
test/jin1.png --> '津'
test/jin4.png --> '晋'
test/jing.png --> '京'
test/k.png --> 'k'
test/l.png --> '1'
test/liao.png --> '辽'
test/lt.png --> '<'
test/lu.png --> '鲁'
test/m.png --> 'm'
test/meng.png --> '晋'
test/min.png --> '闽'
test/n.png --> 'n'
test/ning.png --> '宁'
test/o.png --> 'O'
test/oe.png --> '鄂'
test/p.png --> 'p'
test/percent.png --> '%'
test/point.png --> 'l'
test/q.png --> 'q'
test/qing.png --> '青'
test/qiong.png --> '渝'
test/r.png --> 'r'
test/s.png --> 's'
test/shan.png --> '陕'
test/star.png --> '*'
test/su.png --> '苏'
test/t.png --> 't'
test/u.png --> 'u'
test/v.png --> 'V'
test/w.png --> 'W'
test/wan.png --> '皖'
test/x.png --> 'x'
test/xiang.png --> '湘'
test/xin.png --> '新'
test/y.png --> 'y'
test/yu2.png --> 'm'
test/yu4.png --> 'l'
test/yue.png --> 'P'
test/yun.png --> '云'
test/z.png --> 'z'
test/zang.png --> '藏'
test/zhe.png --> '浙'
```

感知准则函数

```
test/!.png --> '!'
test/#.png --> '-'
test/$.png --> '$'
test/&.png --> '&'
test/+.png --> '*'
test/0.png --> 'O'
test/1.png --> '1'
test/2.png --> '2'
test/3.png --> '冀'
test/4.png --> '4'
test/5.png --> '黑'
test/6.png --> '6'
test/7.png --> '7'
test/8.png --> '8'
test/9.png --> '9'
test/=.png --> '藏'
test/?.png --> '?'
test/AA.png --> '^'
test/BB.png --> 'B'
test/CC.png --> 'C'
test/DD.png --> 'D'
test/EE.png --> 'E'
test/FF.png --> 'F'
test/GG.png --> 'G'
test/HH.png --> 'H'
test/II.png --> 'I'
test/JJ.png --> 'J'
test/KK.png --> 'K'
test/LL.png --> 'L'
test/MM.png --> 'M'
test/NN.png --> 'N'
test/OO.png --> '赣'
test/PP.png --> 'P'
test/QQ.png --> 'Q'
test/RR.png --> 'R'
test/SS.png --> 'S'
test/TT.png --> 'T'
test/UU.png --> 'U'
test/VV.png --> 'V'
test/WW.png --> 'W'
test/XX.png --> 'X'
test/YY.png --> 'Y'
test/ZZ.png --> 'Z'
test/^.png --> '黑'
test/a.png --> 'a'
test/b.png --> 'b'
test/c.png --> 'c'
test/chuan.png --> '川'
test/comma.png --> '!'
test/d.png --> 'd'
test/ee.png --> 'e'
test/f.png --> 'f'
test/g.png --> 'g'
test/gan.png --> '#'
test/gt.png --> '>'
test/gui.png --> '赣'
test/gui4.png --> '!'
test/h.png --> 'h'
test/hei.png --> '-'
test/hg.png --> '-'
test/hu.png --> '!'
test/i.png --> 'l'
test/j.png --> 'J'
test/ji2.png --> '.'
test/ji4.png --> '-'
test/jin1.png --> '津'
test/jin4.png --> '-'
test/jing.png --> '.'
test/k.png --> 'k'
test/l.png --> 'l'
test/liao.png --> '辽'
test/lt.png --> '<'
test/lu.png --> '.'
test/m.png --> 'm'
test/meng.png --> '-'
test/min.png --> '闽'
test/n.png --> 'n'
test/ning.png --> '宁'
test/o.png --> 'O'
test/oe.png --> '鄂'
test/p.png --> 'p'
test/percent.png --> '%'
test/point.png --> '-'
test/q.png --> 'Q'
test/qing.png --> '-'
test/qiong.png --> '.'
test/r.png --> 'r'
test/s.png --> 'S'
test/shan.png --> '赣'
test/star.png --> '*'
test/su.png --> '苏'
test/t.png --> 't'
test/u.png --> 'u'
test/v.png --> 'V'
test/w.png --> 'w'
test/wan.png --> '-'
test/x.png --> 'X'
test/xiang.png --> '湘'
test/xin.png --> 'I'
test/y.png --> 'y'
test/yu2.png --> 'l'
test/yu4.png --> '.'
test/yue.png --> '-'
test/yun.png --> '云'
test/z.png --> 'Z'
test/zang.png --> '-'
test/zhe.png --> '浙'
```

## 联系方式

Li xilin <lihsilyn@gmail.com>

## License

本项目基于 [GLWTPL](https://github.com/me-shaon/GLWTPL) (Good Luck With That Public License) 许可证开源。

